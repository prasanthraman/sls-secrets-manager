import datetime
import json
def event_notify(event, context):
   

    # Print the file name and bucket
    current_time = datetime.datetime.now()
    print(f'I will be invoked every 2 min. Current Time {current_time}')
    return {
        'statusCode': 200,
        'body': f'I will be invoked every 2 min. Current Time {current_time}'
    }