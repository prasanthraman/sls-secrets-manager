import json
import boto3
import os

def get_parameter(event, context):
    parameter_name = "/path/app_id"

    # Create a Systems Manager client
    client = boto3.client('ssm')

    try:
        # Retrieve the parameter value
        response = client.get_parameter(Name=parameter_name)

        # Get the parameter value from the response
        parameter_value = response['Parameter']['Value']
        print(parameter_value)
        app_id = os.environ['APP_ID']
        app_pt = os.environ['APP_PT']
        app_secret = os.environ['APP_SECRET']
        print(parameter_value,app_id,app_pt,app_secret)
        return {
            'statusCode': 200,
            'body': json.dumps({'parameter': parameter_value})
        }
    except Exception as e:
        return {
            'statusCode': 500,
            'body': json.dumps({'error': str(e)})
        }