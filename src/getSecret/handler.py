import boto3
import json
from botocore.exceptions import ClientError


def get_secret(event,context):

    secret_name = "test2"
    region_name = "us-east-1"

    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_name
        )
        secret = get_secret_value_response['SecretString']
        return {
            'statusCode': 200,
            'body': json.dumps({'secret': secret})
        }
    except ClientError as e:
        # For a list of exceptions thrown, see
        # https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
        return {
            'statusCode': 500,
            'body': json.dumps({'error': str(e)})
        }

